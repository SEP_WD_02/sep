﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Common_AboutUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //checking for empty session field
        if (Session["user"] == null)
            //go to common home page
            Response.Redirect("../Common_Home.aspx");

        //assign session variable value to text field
        txtUser.Text = Session["user"].ToString();

        string user = Session["user"].ToString();
        
    }
}
