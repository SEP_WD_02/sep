﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/MainMaster.master" AutoEventWireup="true"
    CodeFile="Profile.aspx.cs" Inherits="Common_AboutUs" Title="Profile Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="Text1" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Medium" ForeColor="White"></asp:Label>
                <br />
                <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Arial" Font-Size="Small"
                    ForeColor="Yellow" NavigateUrl="~/Common_Home.aspx"> Logout</asp:HyperLink>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="Home.aspx">Home</a></li>
                    <li><a href="AboutUs.aspx">About Us</a></li>
                    <li><a href="ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>
         <!--Navigator -->
            <div style="background-color:White; width:958px; height:35px; padding:5px;">
                <div style="height:32px; width:954px; border-color:#0280EC; border-width:1px; border-style:solid; padding:1px;">
                <div class="NaviTabCommon">
                    <asp:HyperLink ID="HyperLink7" runat="server" 
                        NavigateUrl="~/Logged_Pages/Home.aspx" CssClass="HyperLink">Main Home</asp:HyperLink>
                </div>
                <div class="NaviTabFinal">
                    <asp:HyperLink ID="HyperLink8" runat="server" 
                        NavigateUrl="~/Logged_Pages/Profile.aspx" CssClass="HyperLink">Profile</asp:HyperLink>
                </div>
                </div>
            </div>
         <!--Navigator -->
        <div id="prevX">
            <asp:Image ID="Image1" runat="server" Height="212px" ImageUrl="~/Logged_Pages/images/profile-banner.jpg"
                Width="950px" />
        </div>
        <div id="content">
        </div>
        <div class="con_bot">
        </div>
        <div class="gapSet">
        </div>
        <div class="con_top">
        </div>
        <div class="proSubtitle">
            Personal Details             <div class="proRow">
                <div class="RowcolLft">
                    User No</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserNo" runat="server" Text="lblUserNo"></asp:Label>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    User Name</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserName" runat="server" Text="lblUserName"></asp:Label>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    Role</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserRole" runat="server" Text="lblUserRole"></asp:Label>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    Loged time</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserLogtime" runat="server" Text="lblUserLogtime"></asp:Label>
                </div>
            </div>
            <div class="gapSet">
            </div>
            <asp:Label ID="Label1" runat="server" Text="Previous login details :"></asp:Label>
            <div class="proRow">
                <div class="RowcolLft">
                    Loged time</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserPrelogtime" runat="server" Text="lblUserPrelogtime"></asp:Label>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    Loged date</div>
                <div class="RowcolRte">
                    :<asp:Label ID="lblUserPrelogDate" runat="server" Text="lblUserPrelogDate"></asp:Label>
                </div>
            </div>
        </div>
        <div class="proSubtitle">
            User Description
            <div class="TextDiv" style="color: #000000">
                Summary description about the user. This one is generated according to the user 
                role.</div>
        </div>
        <div class="con_bot">
        </div>
        <div class="gapSet">
        </div>
        <div class="con_top">
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
        <div class="proSubtitle">
            Security
            <div class="TextDiv" style="color: #000000">
                User can have their own password And Server allow to users to change thair 
                passwords frequently , for best security.</div>
            <div class="proRow">
                <div class="RowcolLft">
                    Current Password</div>
                <div class="RowcolRte">
                    :<asp:TextBox ID="txtCurPas" runat="server" Height="20px" TextMode="Password" Width="208px"
                        OnTextChanged="txtCurPas_TextChanged" CssClass="commonTextBox" ValidationGroup="ChangePw"></asp:TextBox>
                    &nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCurPas"
                        ErrorMessage="*Incompleted" ValidationGroup="ChangePw"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    New Password</div>
                <div class="RowcolRte">
                    :<asp:TextBox ID="txtNewPass" runat="server" Height="20px" Width="208px" TextMode="Password"
                        CssClass="commonTextBox" ValidationGroup="ChangePw"></asp:TextBox>
                    &nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNewPass"
                        ErrorMessage="*Incompleted" ValidationGroup="ChangePw"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                    Conform Password</div>
                <div class="RowcolRte">
                    :<asp:TextBox ID="txtConfPass" runat="server" Height="20px" TextMode="Password" Width="208px"
                        CssClass="commonTextBox" ValidationGroup="ChangePw"></asp:TextBox>
                    &nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtConfPass" ErrorMessage="*Incompleted" 
                        ValidationGroup="ChangePw"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtConfPass"
                        ErrorMessage="*Paswords Mismatch" ValidationGroup="ChangePw" 
                        ControlToCompare="txtNewPass"></asp:CompareValidator>
                </div>
            </div>
            <div class="proRow">
                <div class="RowcolLft">
                     </div>
                <div class="RowcolRte">
                    &nbsp;&nbsp;
                    <asp:Button ID="btnChange" runat="server" Height="20px" Text="Change Password" Width="139px"
                        OnClick="btnChange_Click" CssClass="buttonText" ValidationGroup="ChangePw" />
                    <asp:Label ID="lblstatus" runat="server" ForeColor="Red"></asp:Label>
                    </div>
            </div>
            
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        
        <div class="con_bot">
        </div>
        <div class="gapSet">
        </div>
        <div class="con_top">
        </div>
        <div class="proSubtitle">
            Functions </div>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="TextDiv" style="color: #000000">
                    According to the User role, access for functions is limited. Here you can see 
                    what are the functions you can handle using this web server.<br />
                    <br />
                    <asp:Label ID="lblShowmore" runat="server" Text="See more"></asp:Label>
                    <asp:ImageButton ID="ImgBtnShow" runat="server" Height="24px" ImageUrl="~/Logged_Pages/images/next.png"
                        OnClick="ImgBtnShow_Click" Width="24px" />
                    &nbsp;<div>
                        <asp:Panel ID="PnlFunction" runat="server" BackImageUrl="~/Logged_Pages/images/HighAdminFun.jpg"
                            Visible="False">
                            <asp:Image ID="ImgFun" runat="server" Height="366px" ImageUrl="~/Logged_Pages/images/HighAdminFun.jpg"
                                Width="905px" />
                        </asp:Panel>
                    </div>
                    <div>
                        <asp:Label ID="lblhide" runat="server" Text="Hide view"></asp:Label>
                        <asp:ImageButton ID="ImgBtnHide" runat="server" Height="24px" ImageUrl="~/Logged_Pages/images/hide.png"
                            OnClick="ImgBtnHide_Click" Width="26px" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <!-- Extra features Start-->
        <div style="height: 17px; width: 968px; background-image: url('images/LowerSlider.jpg');">
        </div>
    <div style="height: 200px; width: 968px; background-image: url('images/register.jpg');">
        <div style="float: left; width: 220px; height: 195px; padding-left: 50px;">
            <asp:Panel ID="Panel1" runat="server" Height="195px" Width="255px">
                <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" 
                    LineImagesFolder="~/TreeLineImages">
                    <ParentNodeStyle Font-Bold="False" />
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                        VerticalPadding="0px" ForeColor="#5555DD" />
                    <Nodes>
                        <asp:TreeNode Expanded="True" NavigateUrl="~/Logged_Pages/Home.aspx" Text="Home"
                            Value="Home">
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/Academic_Home.aspx" Text="Academic Page"
                                Value="Academic Page"></asp:TreeNode>
                            <asp:TreeNode Text="Non-Academic Page" Value="Non-Academic Page"></asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/Parents_Home.aspx" Text="Parents Page"
                                Value="Parents Page"></asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/StudentHome.aspx" Text="Student Page" Value="Student Page">
                            </asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Logged_Pages/AboutUs.aspx" Text="About us" Value="About us">
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Logged_Pages/ContactUs.aspx" Text="Contact us" Value="Contact us">
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Common_Home.aspx" Text="Logout" Value="Logout"></asp:TreeNode>
                    </Nodes>
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Blue" HorizontalPadding="5px"
                        NodeSpacing="0px" VerticalPadding="0px" CssClass="HyperLink" />
                </asp:TreeView>
            </asp:Panel>
        </div>
        <!--Security declaration -->
        <div style="float: left; width: 435px; height: 195px; padding-left: 20px;">
            <div id="SecurityDiv" style="width: 270px; height: 195px; padding-left: 155px;">
                <ui>
                    <li><b>Authenticity: </b>Website provides you unique username &amp; password. Also you 
                        can change your current password frequently. It will prevent your account from 
                        unauthorized access. Visit your profile page for more details..<asp:HyperLink 
                            ID="HyperLink6" runat="server" Font-Names="Arial" Font-Size="11px" 
                            Font-Underline="True" ForeColor="#FF6600" 
                            NavigateUrl="~/Logged_Pages/Profile.aspx">Visit Profile</asp:HyperLink></li>
                            
                    <li><b>Authorization: </b>The web server keeps few user levels to establish 
                        confidentiality between each user’s duties. So that users are independent to do 
                        their own work..</li>
                    </ui>
            </div>
        </div>
        <div style="float: left; width: 230px; height: 195px; padding-left: 5px;">
            <div style="float: left; width: 220px; height: 20px; text-align: center; font-size: 17px;
                padding: 5px; font-weight: bold;">
                Useful Resources
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/UGC.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.ugc.ac.lk/" CssClass="HyperLink">Sri Lanka 
                    University Grand Commission</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/NIE.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.nie.lk/" CssClass="HyperLink">Sri Lanka National 
                    Institute of Education</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/SED.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.doenets.lk/exam/" CssClass="HyperLink">Department of 
                    Examination Sri Lanka</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/FB.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="12px" 
                        NavigateUrl="https://www.facebook.com/" CssClass="HyperLink">School Info 
                    Management System - Face Book</asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
    <!-- Extra features Ends-->
        <div class="con_bot">
        </div>
        <div id="footer">
            <p>
                All Rights Reserved</p>
            <!-- Please DO NOT remove the following notice -->
            <p>
                Designed by Sri Lanka Institute of Information Technology (Software Engineering
                Batch – week day group No: 2)</p>
            <!-- end of copyright notice-->
        </div>
</asp:Content>
