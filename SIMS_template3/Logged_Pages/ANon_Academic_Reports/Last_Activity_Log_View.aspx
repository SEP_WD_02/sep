﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/ReportTemplate_n.master" AutoEventWireup="true"
    CodeFile="Last_Activity_Log_View.aspx.cs" Inherits="Logged_Pages_AParents_Reports_Parent_Info"
    Title="General Academic" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../../Confirmation.css" rel="stylesheet" type="text/css" />

    <link href="../../Notifications/CSS/Style.css" rel="stylesheet" type="text/css" />

    <script src="../../lib/jquery-1.5.1.js" type="text/javascript"></script>

    <script src="../../lib/Jquery-Impromptu.3.3.js" type="text/javascript"></script>

    <script src="../../lib/DeepASPImpromptuCalling.js" type="text/javascript"></script>
         <title>Calendar Extender</title>
    
    <script type="text/javascript">

        var message = "Function Disabled!";
        function clickIE4() {
            if (event.button == 2) {
                alert(message);
                return false;
            }
        }
        function clickNS4(e) {
            if (document.layers || document.getElementById && !document.all) {
                if (e.which == 2 || e.which == 3) {
                    alert(message);
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
            document.onmousedown = clickNS4;
        }
        else if (document.all && !document.getElementById) {
            document.onmousedown = clickIE4;
        }
        document.oncontextmenu = new Function("alert(message);return false")
</script>
    

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="Text1" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Small" ForeColor="White"></asp:Label>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="../Home.aspx">Home</a></li>
                    <li><a href="../AboutUs.aspx" class="active">About Us</a></li>
                    <li><a href="../ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <div class="ReportBody">
                    <div class="ReportHeader">
                      Student Extra Curricular Activity Details
                    </div>
                    <div class="ReportDesc">
                         View Student Extra Curricular Activity Details
                    </div>
                </div>
                <div class="FormHeadder">
                </div>
                <div class="FormBackgroundAddAppointmentHesh">

                    <div class="FormRow">
                        <div class="fieldsCaptionCell3">                       
                        </div>
                        <div class="fieldsElementCell3">
                        </div>
                        <div class="fieldsCaptionCell3">                           
                        </div>
                        <div class="fieldsElementCell3">
                        </div>
                        <asp:TextBox ID="txt_search" runat="server" Width="182px" 
                            Class="FormTxtBoxHesh " MaxLength="10"></asp:TextBox>
                            
                        <asp:Button ID="btn_search" runat="server" Height="26px" Text="Search" Width="100px"
                                 CssClass="buttonText"  Font-Bold="True" onclick="btn_search_Click" />
                        
                    </div>
                    <div id="dateField" style="display: none; padding-left: 671px">
                        
                    </div>
                  
                                        <div class="FormRow">
                        <div class="fieldsCaptionCell3">
                           
                        </div>
                        <div class="fieldsElementCell3">

                        </div>
                        <div class="fieldsCaptionCell3">
                            
                        </div>
                        <div class="fieldsElementCell3">

                        </div>
                     
                                                            <asp:Label ID="lbl_status"  ForeColor="Red" class="RLable" runat="server" Font-Bold="True" Text=" "
                                                     Font-Names="Arial"></asp:Label>
                                                     

                        
                    </div>
                      
                    <br />
                </div>
                <div class="FormBackgroundAddAppointment">
<asp:GridView ID="gv_UD_activity" runat="server" Height="401px" Width="94.5%" AutoGenerateColumns="False"
                        BorderColor="White" BorderStyle="Ridge" CellSpacing="1" CellPadding="3" GridLines="Horizontal"
                        BackColor="White" BorderWidth="2px" 
                        EmptyDataText="No Matching Records Found" DataKeyNames="ADMISSION_NO"
                        Font-Names="Arial" Font-Size="Small" AllowPaging="True" PageSize="10" 
                        ShowFooter="false" ForeColor="Black" onpageindexchanging="gv_UD_activity_PageIndexChanging"
                       >
                        <PagerSettings PageButtonCount="3" />
                        <EmptyDataRowStyle Font-Names="Arial" Font-Size="Large" ForeColor="Red" />
                        <Columns>
                    <asp:BoundField DataField="ADMISSION_NO" HeaderText="Admission No" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="110px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ACTIVITY_CODE" HeaderText="Activity Code" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="ACTIVITYSUB_CODE" HeaderText="Activity" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                          
                   <asp:BoundField DataField="ACTIVITY_YEAR" HeaderText="Activity Year" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                           
                   <asp:BoundField DataField="ACTIVITY_DESCRIPTION" HeaderText="Activity Description" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="MEMBERSHIP_NO" HeaderText="Membership No" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="DATE_OF_REGISTRATION" HeaderText="Registered Date" 
                    ReadOnly="true">
                    <ControlStyle Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    </asp:BoundField>
                   
                        </Columns>
                        <FooterStyle ForeColor="#0066FF" />
                        <PagerStyle ForeColor="#0066FF" />
                        <HeaderStyle BackColor="#333399" ForeColor="White" />
                        <AlternatingRowStyle BackColor="#CCFFF7" />
                    </asp:GridView>
                </div>

        </ContentTemplate>
        </asp:UpdatePanel>
        <div class="FormFooter">
        </div>
    </div>
</asp:Content>
