﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using CrystalDecisions.CrystalReports.Engine;
using System.Drawing;

public partial class Logged_Pages_AParents_Reports_Parent_Info : System.Web.UI.Page
{
    //define the path to datase
    string Path = "Dsn=nsis;uid=root;pwd=123";
    //define comman variables for report
    static ReportDocument rd;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                //invoke the activity_dd2 filling methode
                fill_ddl1();
                fill_all_years();
                fill_ddl_activity();
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Page Can Not Be Loaded.')", true);
            }
        }
    }
    //fill the base type drop down list
    public void fill_ddl1()
    {
        ddl_catagory.Items.Insert(0, "Select Type");
        ddl_catagory.Items.Insert(1, "Sport Wise");
        ddl_catagory.Items.Insert(2, "Extra Activty Wise");
        ddl_catagory.DataBind();
    }

    public void fill_all_years()
    {
        DataTable dt = new DataTable();
        try
        {
            if (get_all_years_().Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There are no Sufficient Records In Order To Generate The Report')", true);
            }

            ddl_year.DataSource = get_all_years_();
            ddl_year.DataValueField = "ACTIVITY_YEAR";
            ddl_year.DataMember = "ACTIVITY_YEAR";
            ddl_year.DataBind();
            ddl_year.Items.Insert(0, "Select Year");
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Is Error In Retriving Activity Years, Reload The Page')", true);
        }
    }
    //get all information about sports
    public DataTable get_all_sports_Details()
    {
        try
        {
            DataTable dt_allsports;
            //create new connection with the databse
            OdbcConnection con = new OdbcConnection(Path);
            //open the settedup connection
            con.Open();
            //retrive data throught the adpter using quary
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from extra_activity where ACTIVITY_CODE='Sports'", con);
            //define new data table object
            dt_allsports = new DataTable();
            //fill the databale object with retrived data
            adapter.Fill(dt_allsports);
            //close the settedup connection
            con.Close();
            //return the filled data table object
            return dt_allsports;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('No records under sport catagory')</script>");

        }
        return null;
    }
    //get all information about extraactivites
    public DataTable get_all_extraactivities_details()
    {
        try
        {
            DataTable dt_extraactivity;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from extra_activity where ACTIVITY_CODE='Extra'", con);
            dt_extraactivity = new DataTable();
            adapter.Fill(dt_extraactivity);
            con.Close();
            return dt_extraactivity;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('No records under extra activity catagory')</script>");

        }
        return null;
    }
    //get all information about student extracurricular activities
    public DataTable get_extraacurricular()
    {
        try
        {
            DataTable dt_extraactivity;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select distinct ACTIVITYSUB_CODE from extra_activity ", con);
            dt_extraactivity = new DataTable();
            adapter.Fill(dt_extraactivity);
            con.Close();
            return dt_extraactivity;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('No records under extra activity catagory')</script>");

        }
        return null;
    }

    //get all sport years 
    public DataTable get_all_years_()
    {
        try
        {
            DataTable dt_extraactivity_years;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select distinct ACTIVITY_YEAR from extra_activity ", con);
            dt_extraactivity_years = new DataTable();
            adapter.Fill(dt_extraactivity_years);
            con.Close();
            return dt_extraactivity_years;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('No records under extra activity catagory')</script>");

        }
        return null;

    }
 
    public void fill_ddl_activity()
    {
        try
        {
            if (get_extraacurricular().Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Are No Sufficient Data To Generate Record')", true);
            }
            else
            {
                ddl_activity.DataSource = get_extraacurricular();
                ddl_activity.DataMember = "ACTIVITYSUB_CODE";
                ddl_activity.DataValueField = "ACTIVITYSUB_CODE";
                ddl_activity.DataBind();
                ddl_activity.Items.Insert(0, "Select Activity");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Is Error In Report, Reload The Page')", true);
        }
    }


    protected void ddl_year_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lbl_activity_check.Text = string.Empty;
            lbl_catagory_check.Text = string.Empty;

            rd = new ReportDocument();
            ////load the report from the selected location
            rd.Load("C:\\Users\\Neon\\Desktop\\Final ready 2nd releasePresentation\\SIMS_template3\\CrystalReport_chart_year.rpt");

            ////print report details
            TextObject txt_topic = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
            txt_topic.Text = "Student Participation in Year " + ddl_year.Text.ToString();
            ////get_report_extravise(rd, ddl_chartsSubbase.Text);

            get_chart_with_year(rd, ddl_year.Text);
            //lbl_year_check.Text = "ok";
            crvReportViewercharts.ReportSource = rd;
            crvReportViewercharts.RefreshReport();
            ddl_year.Items.Clear();
            fill_all_years();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Is Error In Report, Reload The Page')", true);
        }
    }
    protected void ddl_activity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lbl_catagory_check.Text = string.Empty;
            lbl_year_check.Text = string.Empty;
            rd = new ReportDocument();
            rd.Load("C:\\Users\\Neon\\Desktop\\Final ready 2nd releasePresentation\\SIMS_template3\\CrystalReport_sport_with_year.rpt");
            TextObject txt_topic = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
            txt_topic.Text = "Student Participation in  " + ddl_activity.Text.ToString();
            get_chart_with_Activity(rd, ddl_activity.Text);
            crvReportViewercharts.ReportSource = rd;
            crvReportViewercharts.RefreshReport();
            ddl_activity.Items.Clear();
            fill_ddl_activity();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Is Error In Report, Reload The Page')", true);
        }
    }
    public void getreport(ReportDocument rd)
    {
        try
        {
            DataSet ds_sport = getsport();
            if (ds_sport.Tables.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There are no sufficient records to generate the report')", true);
            }
            else
            {
                rd.SetDataSource(ds_sport);
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Required records can not be retrived')", true);
        }
    }
    //draw the report with relevant filterings
    public void get_chart_extra(ReportDocument rd)
    {
        try
        {
            DataSet ds_extra = getextra();
            if (ds_extra.Tables.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There are no sufficient records to generate the report')", true);
            }
            else
            {
                rd.SetDataSource(ds_extra);

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Required records can not be retrived')", true);
        }
    }

    //build dataset with relevant information
    public DataSet getsport()
    {
        try
        {
            DataSet dt;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select ADMISSION_NO,ACTIVITYSUB_CODE,ACTIVITY_YEAR FROM EXTRA_ACTIVITY where ACTIVITY_CODE='Sports'", con);
            dt = new DataSet();
            adapter.Fill(dt, "extra_activity");
            con.Close();
            return dt;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }
        return null;
    }
    //build dataset with relevant information accrding to extra activites
    public DataSet getextra()
    {
        try
        {
            DataSet dt;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select ADMISSION_NO,ACTIVITYSUB_CODE,ACTIVITY_YEAR FROM EXTRA_ACTIVITY where ACTIVITY_CODE='Extra'", con);
            dt = new DataSet();
            adapter.Fill(dt, "extra_activity");
            con.Close();
            return dt;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }
        return null;
    }

    //build dataset with relevant information accrding to sports and according to year
    public DataSet getsport_with_Activity(string activity)
    {
        try
        {
            DataSet dt;
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * FROM EXTRA_ACTIVITY where ACTIVITYSUB_CODE='" + activity + "'", con);
            dt = new DataSet();
            adapter.Fill(dt, "extra_activity");
            con.Close();
            return dt;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }
        return null;
    }
    //build dataset with relevant information accrding to sports and according to year
    public DataSet getsport_with_year(string year)
    {
        try
        {
            DataSet dt;
            decimal years = Convert.ToDecimal(year);
            OdbcConnection con = new OdbcConnection(Path);
            con.Open();
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * FROM EXTRA_ACTIVITY where ACTIVITY_YEAR=" + years + "", con);
            dt = new DataSet();
            adapter.Fill(dt, "extra_activity");
            con.Close();
            return dt;
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }
        return null;
    }

    //draw the report with relevant filterings
    public void get_chart_with_Activity(ReportDocument rd, string acivity)
    {
        try
        {
            DataSet ss = getsport_with_Activity(acivity);
            rd.SetDataSource(ss);
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }
    }
    public void get_chart_with_year(ReportDocument rd, string year)
    {
        try
        {
            DataSet ss = getsport_with_year(year);
            rd.SetDataSource(ss);
        }
        catch
        {
            //prompt an error message if any exception occurs
            Response.Write("<script>alert('Error in Processing, Try again')");
        }

    }


    protected void ddl_catagory_SelectedIndexChanged(object sender, EventArgs e)
    {

        lbl_year_check.Text = string.Empty;
        lbl_activity_check.Text = string.Empty;
        try
        {
            if (ddl_catagory.Text == "Select Type")
            {
                lbl_catagory_check.Text = "Invalid Catagory";
            }
            else
            {
                if (ddl_catagory.Text == "Extra Activty Wise")
                {
                    rd = new ReportDocument();
                    rd.Load("C:\\Users\\Neon\\Desktop\\Final ready 2nd releasePresentation\\SIMS_template3\\CrystalReport_chart_full.rpt");
                    TextObject txt_topic = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txt_topic.Text = "Student Participation in Extra Curricular activities";
                    get_chart_extra(rd);

                }
                else if (ddl_catagory.Text == "Sport Wise")
                {
                    rd = new ReportDocument();
                    rd.Load("C:\\Users\\Neon\\Desktop\\Final ready 2nd releasePresentation\\SIMS_template3\\CrystalReport_chart_full.rpt");
                    TextObject txt_topic = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txt_topic.Text = "Student Participation in Sports";
                    getreport(rd);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Operation can not be fulfill')", true);
                }
                crvReportViewercharts.ReportSource = rd;
                crvReportViewercharts.RefreshReport();
                ddl_catagory.Items.Clear();
                fill_ddl1();
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning !! There Is Error In Report, Reload The Page')", true);
        }
    }
   
}
