<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/ReportTemplate_n.master" AutoEventWireup="true"
    CodeFile="Last_Student_Participation_Certificate.aspx.cs" Inherits="Logged_Pages_AParents_Reports_Parent_Info"
    Title="Add to Photo Gallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <link href="../styles.css" rel="stylesheet" type="text/css" />

 
    
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="Text1" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Small" ForeColor="White"></asp:Label>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="../Home.aspx">Home</a></li>
                    <li><a href="../AboutUs.aspx" class="active">About Us</a></li>
                    <li><a href="../ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>

                <div class="ReportBody">
                    <div class="ReportHeader">
                        Charts in Student Participation In Extra-Curricullar Acticities
                    </div>
                    <div class="ReportDesc">
                        Generate Chart
                    </div>
                </div>
                <div class="FormHeadder">
                </div>
                <div class="FormBackgroundAddAppointment">
                    <asp:ToolkitScriptManager ID="ToolkitScriptManager3" runat="server">
                    </asp:ToolkitScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel_leave" runat="server">
                    <ContentTemplate>
                    
                                                              <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="headin01" runat="server" class="RLable" ForeColor="Blue" Text="Based In Catagory" 
                                Font-Bold="True"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">

                            <asp:Label ID="headin2" runat="server" class="RLable" ForeColor="Blue" 
                                Text="Based In Year" Font-Bold="True"></asp:Label>

                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">

                                
                            <asp:Label ID="headin3" runat="server" class="RLable" ForeColor="Blue" 
                                Text="Based In Activity" Font-Bold="True"></asp:Label>

                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                                          <div class="FormRow">
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label1" runat="server" class="RLable" Text="Category :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox " ID="ddl_catagory" 
                                runat="server" Height="20px" Width="185px"
                     
                                AutoPostBack="true" onselectedindexchanged="ddl_catagory_SelectedIndexChanged"  
                               >

                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label2" runat="server" class="RLable" Text="   Year :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox " ID="ddl_year" 
                                runat="server" Height="20px" Width="185px"
                     
                                AutoPostBack="true" onselectedindexchanged="ddl_year_SelectedIndexChanged" 
                               >

                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label14" runat="server" class="RLable" Text="Activty :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                           <asp:DropDownList class="RTxt" CssClass="FormTxtBox "  ID="ddl_activity" 
                                runat="server" Height="20px" Width="185px"
                                   AutoPostBack="true" onselectedindexchanged="ddl_activity_SelectedIndexChanged" 
                               >
                              
                                </asp:DropDownList>
                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                                          <div class="FormRow">
                        <div class="fieldsCaptionCell">
                                          
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="lbl_catagory_check" runat="server" class="RLable" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="lbl_year_check"  ForeColor="Red" class="RLable" runat="server"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="lbl_activity_check"  ForeColor="Red" class="RLable" runat="server"></asp:Label>
                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>




                    <div class="paraSpace">
                    </div>
                          <div class="RepPanelauto">
                        <CR:CrystalReportViewer  ID="crvReportViewercharts" runat="server" AutoDataBind="true"
                            DisplayGroupTree="False" EnableDatabaseLogonPrompt="False" 
                                  EnableParameterPrompt="False" DisplayToolbar="False" />
                    </div>
                     <div class="paraSpace">
                    </div>
                    </ContentTemplate>

                    </asp:UpdatePanel>





                </div>
                <div class="FormFooter">
                </div>

    </div>
</asp:Content>
