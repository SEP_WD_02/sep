﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/ReportTemplate_n.master" AutoEventWireup="true"
    CodeFile="Last_Activity_log.aspx.cs" Inherits="Logged_Pages_AParents_Reports_Parent_Info"
    Title="Add to Photo Gallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <link href="../styles.css" rel="stylesheet" type="text/css" />
    <link href="Heshan_Nortification/Style.css" rel="stylesheet" type="text/css" />
    <script src="Heshan_Nortification/Jquery-Impromptu.3.3.js" type="text/javascript"></script>
    <script src="Heshan_Nortification/jquery-1.5.1.js" type="text/javascript"></script>
    <script src="Heshan_Nortification/DeepASPImpromptuCalling.js" type="text/javascript"></script>



     <script type="text/javascript">
    function checkDate(sender,args)
{
 if (sender._selectedDate > new Date())
            {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(); 
                // set the date back to the current date
sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
}
    </script>
    
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="Text1" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Small" ForeColor="White"></asp:Label>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="../Home.aspx">Home</a></li>
                    <li><a href="../AboutUs.aspx" class="active">About Us</a></li>
                    <li><a href="../ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>

                <div class="ReportBody">
                    <div class="ReportHeader">
                        Extra Curricular Activity Log
                    </div>
                    <div class="ReportDesc">
                        Add Student Extra Crricular Record
                    </div>
                </div>
                <div class="FormHeadder">
                </div>
                <div class="FormBackgroundAddAppointment">

                    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
                    </asp:ToolkitScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                                            <div class="FormRow">
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label1" runat="server" class="RLable" Text="Grade :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox "  ID="ddl_grade" 
                                runat="server" Height="20px" Width="185px"
                                    AutoPostBack="true" onselectedindexchanged="ddl_grade_SelectedIndexChanged" 
                                 >           
                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label2" runat="server" class="RLable" Text="Class :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox " ID="ddl_class" 
                                runat="server" Height="20px" Width="185px"
                     
                                AutoPostBack="true" onselectedindexchanged="ddl_class_SelectedIndexChanged" 
                                >

                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label14" runat="server" class="RLable" Text="Student :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                           <asp:DropDownList class="RTxt" CssClass="FormTxtBox "  ID="ddl_student" 
                                runat="server" Height="20px" Width="185px"
                                   AutoPostBack="true" onselectedindexchanged="ddl_student_SelectedIndexChanged" 
                                >
                              
                                </asp:DropDownList>
                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                       <div class="FormRow">
                       <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
                        <asp:Label ID="lbl_grade_check" runat="server" class="RLable" ForeColor="Red" 
                                Width="195px"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
                        <asp:Label ID="lbl_class_check"  ForeColor="Red" class="RLable" runat="server" 
                                Width="175px"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">


                                
                            <asp:Label ID="lbl_student_check" runat="server" class="RLable" ForeColor="Red"></asp:Label>


                                
                        </div>
                    </div>

                    <div class="paraSpace">
                    </div>
                                            <div class="FormRow">
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label4" runat="server" class="RLable" Text="Activity Type :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox "  ID="ddl_type" 
                                runat="server" Height="20px" Width="185px"
                                    AutoPostBack="true" onselectedindexchanged="ddl_type_SelectedIndexChanged" 
                                >           
                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label5" runat="server" class="RLable" Text="Activity :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox " ID="ddl_activity" 
                                runat="server" Height="20px" Width="185px"
                     
                                AutoPostBack="true" onselectedindexchanged="ddl_activity_SelectedIndexChanged" 
                               >

                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label6" runat="server" class="RLable" Text="Activity Year :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
<asp:TextBox ID="txt_year" runat="server" CssClass="FormTxtBox "  Width="185px" MaxLength="4"></asp:TextBox>
                                
                        </div>
                    </div>
                                        <div class="paraSpace">
                    </div>
                                                              <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="lbl_type_check" runat="server" class="RLable" ForeColor="Red" Width="175px"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
<asp:Label ID="lbl_activity_check"  ForeColor="Red" class="RLable" runat="server"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">
                                
                        </div>
                    </div>
                                                            <div class="paraSpace">
                    </div>

                      <div class="FormRow">
                        <div class="fieldsCaptionCell">
                             <asp:Label ID="Label7" runat="server" class="RLable" Text="Reg Date :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:TextBox ID="txt_date" runat="server" Class="FormTxtBox " 
                                Font-Names="Arial" Width="165px" MaxLength="10"></asp:TextBox>
                            <asp:TextBoxWatermarkExtender ID="wme_date" runat="server" TargetControlID="txt_date"
                                WatermarkText="Select Date">
                            </asp:TextBoxWatermarkExtender>
                            <asp:CalendarExtender ID="ce_warning" runat="server" TargetControlID="txt_date" PopupButtonID="ibtn_warning" OnClientDateSelectionChanged="checkDate">
                            </asp:CalendarExtender>
                            <asp:ImageButton ID="ibtn_warning" runat="server" Height="16px" ImageUrl="~/images/Calendar_scheduleHS.png"
                                Width="16px" />
                        </div>
                        <div class="fieldsCaptionCell">
                         
                        </div>
                        <div class="fieldsElementCell">


                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">

                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                                                              <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">
                                
                        </div>
                    </div>
                                                            <div class="paraSpace">
                    </div>
                                        <div class="field_3_Row162app">
                        <div class="fieldsCaptionCell162app">
                            <asp:Label ID="Label15" runat="server" class="RLable" Text="Note :"></asp:Label>
                        </div>
                        <asp:TextBox ID="txt_note" Class="FormTxtBox " runat="server" Height="160px" TextMode="MultiLine"
                            Width="669px" MaxLength="500" ontextchanged="txt_note_TextChanged"></asp:TextBox>
                    </div>
                                        <div class="paraSpace">
                    </div>
                                        <div class="paraSpace">
                    </div>
                                          <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">
                                                    <asp:Button ID="btn_insert" runat="server" Text="Insert" Height="30px" Width="142px"
                               CssClass="buttonText" ValidationGroup="submit" onclick="btn_insert_Click" />
                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">
                                                    <asp:Button ID="btn_clear" runat="server" Height="30px" Width="142px" Text="Clear"
                                CssClass="buttonText" onclick="btn_clear_Click" />

                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">

                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                                        <div class="field_3_Row">
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">

                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                    </div>
                          <div class="RepPanelauto">

                    </div>
                     <div class="paraSpace">
                    </div>

                    </ContentTemplate>

                    </asp:UpdatePanel>





                </div>
                <div class="FormFooter">
                </div>

    </div>
</asp:Content>
