﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;

public partial class Logged_Pages_AParents_Reports_Parent_Info : System.Web.UI.Page
{
    Last_Event lv = new Last_Event();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                fill_type();
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Page can not be loaded')", true);
            }
        }
    }
    public int checkdate(string date)
    {
        int a = 0;
        try
        {
            int year = Convert.ToInt16(date.Substring(6, 2));
            if (date.Length > 10 || date.Length < 8)
            {
                a = 0;
            }
            else if (!date.Contains('/'))
            {
                a = 0;
            }
            else if (Convert.ToInt16(date.Substring(0, 1)) > 12)
            {
                a = 0;
            }
            else if (year > 99)
            {
                a = 0;
            }

            else
            {
                a = 1;
            }
            return a;
        }
        catch
        {
            return a;
        }
    }
    public string note()
    {
        if (txt_description.Text == string.Empty || txt_description.Text == null)
        {
            return "Null";
        }
        else
        {
            return txt_description.Text.Trim();
        }
    }
    public string issues()
    {
        if (txt_issues.Text == string.Empty || txt_issues.Text == null)
        {
            return "Null";
        }
        else
        {
            return txt_issues.Text.Trim();
        }
    }
    protected void btn_clear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        txt_date.Text = string.Empty;
        txt_description.Text = string.Empty; ;
        txt_eventname.Text = string.Empty; ;
        txt_issues.Text = string.Empty;
        txt_organizer.Text = string.Empty;
        ddl_catagoty.Items.Clear();
        lbl_student_check.Text = string.Empty;
        lbl_class_check.Text = string.Empty;
        fill_type();

    }

    public void fill_type()
    {
        try
        {
            DataTable dt_type = lv.get_event_master();
            if (dt_type.Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! No event types can be found')", true);

            }
            else
            {
                ddl_type.DataSource = dt_type;
                ddl_type.DataMember = "emast";
                ddl_type.DataValueField = "emast";
                ddl_type.DataBind();
                ddl_type.Items.Insert(0, "Select Type");
            }
        }
        catch
        {
             ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! No event types can be found')", true);
        }
    }

    public void fill_event_sub_types(string type)
    {
         try
        {
            DataTable dt_sub = lv.get_subevents(type);
            if (dt_sub.Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! No event catagories can be found')", true);
                ddl_catagoty.Items.Clear();

            }
            else
            {
                ddl_catagoty.DataSource = dt_sub;
                ddl_catagoty.DataMember = "esub";
                ddl_catagoty.DataValueField = "esub";
                ddl_catagoty.DataBind();
                ddl_catagoty.Items.Insert(0, "Select Catagory");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! No event catagories  can be found')", true);
        }
    }

    public bool validate_name(string name)
    {
        bool st = false;
        char[] ename = null;
        ename = name.ToCharArray();
        for (int a = 0; a < ename.Length; a++)
        {
            if ((int)(ename[a]) >= 65 && (int)(ename[a]) <= 122 || (int)ename[a] == 32)
            {
                st = true;
            }
            else
            {
                return false;
            }
        }
        return st;
    }

    public bool validate_organizer(string organizer)
    {
        bool st = false;
        char[] ename = null;
        ename = organizer.ToCharArray();
        for (int a = 0; a < ename.Length; a++)
        {
            if ((int)(ename[a]) >= 65 && (int)(ename[a]) <= 122 || (int)ename[a] == 32)
            {
                st = true;
            }
            else
            {
                return false;
            }
        }
        return st;
    }
    protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //ddl_type.Items.Clear();
            ddl_catagoty.Items.Clear();
            lbl_student_check.Text = string.Empty;
            lbl_class_check.Text = string.Empty;
            if (ddl_type.Text == "Select Type")
            {
                lbl_class_check.Text = "Invalid Type";
            }
            else
            {
               fill_event_sub_types(ddl_type.Text);
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Selected Event Type is not valid')", true);
        }
    }
    protected void ddl_catagoty_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lbl_student_check.Text = string.Empty;
            lbl_class_check.Text = string.Empty;
            if (ddl_catagoty.Text == "Select Catagory")
            {
                lbl_student_check.Text = "Invalid Catagoty";
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Selected Event Catagoty is not valid')", true);
        }
    }
    protected void btn_insert_Click(object sender, EventArgs e)
    {
        try
        {



            string name = txt_eventname.Text.Trim();
            string des = note();
            string issue = issues();
            if (txt_eventname.Text == string.Empty || validate_name(name) == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! Please insert valid event name')", true);
            }
            else if (txt_date.Text == string.Empty || checkdate(txt_date.Text) == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! Please Select valid event date')", true);
            }
            else if (txt_organizer.Text == string.Empty || validate_organizer(txt_organizer.Text.Trim()) == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! Please insert valid Organizer')", true);
            }
            else
            {
                if (lv.insert_record(set_event_id(ddl_type.Text,txt_date.Text), ddl_type.Text, ddl_catagoty.Text, name, txt_date.Text, des, txt_organizer.Text, issue))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Success!! New event record addedd successfully')", true);
                    clear();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! Please Provide valid inputs')", true);
                    clear();
                }
            }


        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!!' Event record can not be addedd)", true);
        }
        clear();

    }

    public string set_event_id(string type, string date)
    {
        string id = null;
        try
        {
            string partone = type.Substring(0, 2);        
            string third = partone.Insert(2, date);
            lbl_class_check.Text = third;
            id = third;
            return id;
        }
        catch
        {
            return id;
        }
    }
}
