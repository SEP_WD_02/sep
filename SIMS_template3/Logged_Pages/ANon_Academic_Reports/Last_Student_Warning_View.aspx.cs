﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;

public partial class Logged_Pages_AParents_Reports_Parent_Info : System.Web.UI.Page
{
    /// <summary>
    /// Author - E.A Heshan Sandeepa IT 10 1598 04
    /// 2012-08-16
    /// this is the comman variable to access the database
    /// </summary>
    Last_Warning_View lwv = new Last_Warning_View();
    /// <summary>
    /// when page loading all the records in the table would be there
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                fill();
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Page Can not be Loaded')", true);
            }
        }
    }

    public void fill()
    {
        try
        {
            gv_UD_Warning.DataSource = lwv.databind();
            gv_UD_Warning.DataBind();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Student Warning Details Can not be Loaded)", true);
        }
    }


    protected void gv_UD_Warning_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_UD_Warning.PageIndex = e.NewPageIndex;
        fill();
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (txt_search.Text == string.Empty || txt_search.Text == null)
        {

            lbl_status.Text = "Please Insert Search Key";
            
        }
        else
        {
            try
            {
                gv_UD_Warning.DataSource = lwv.search_results(txt_search.Text.Trim());
                gv_UD_Warning.DataBind();
                lbl_status.Text = string.Empty;
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Incorrect Search Key')", true);
                lbl_status.Text = string.Empty;
            }
           
        }
    }
}
