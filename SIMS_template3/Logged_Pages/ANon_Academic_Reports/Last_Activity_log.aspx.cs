﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using DeepASP.JQueryPromptuTest;

public partial class Logged_Pages_AParents_Reports_Parent_Info : System.Web.UI.Page
{
    //common variable for database access
    Log ialdc = new Log();
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                //invoke the dropdowwn list filling methdodes
                set_grade();

            }
            catch
            {
                //popout custom message if any error happans
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Page Can Not Be Loaded.')", true);
            }
        }
    }
    //prepare the grade data table
    public void set_grade()
    {
        try
        {
            DataTable dt_grade = ialdc.get_all_grades();
            if (dt_grade.Rows.Count == 0)
            {
                lbl_grade_check.Text = "No selected grades";
                clear();
                lbl_class_check.Text = string.Empty;
            }
            else
            {
                ddl_grade.DataSource = dt_grade;
                ddl_grade.DataMember = "SC_GRADE";
                ddl_grade.DataValueField = "SC_GRADE";
                ddl_grade.DataBind();
                ddl_grade.Items.Insert(0, "Select Grade");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! There are no grade records in the database')", true);
        }
    }

    public void fill_class(decimal grade)
    {
        try
        {
            DataTable dt_class = ialdc.get_class_relevaent_grade(grade);
            if (dt_class.Rows.Count == 0)
            {
                lbl_grade_check.Text = "No selected class for the grade";
                ddl_class.Items.Clear();
                ddl_student.Items.Clear();
                lbl_student_check.Text = string.Empty;

            }
            else
            {
                lbl_grade_check.Text = string.Empty;
                lbl_class_check.Text = string.Empty;
                lbl_student_check.Text = string.Empty;
                ddl_class.DataSource = dt_class;
                ddl_class.DataMember = "CLASS_CODE";
                ddl_class.DataValueField = "CLASS_CODE";
                ddl_class.DataBind();
                ddl_class.Items.Insert(0, "Select Class");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! There are no any classes for the selected grade')", true);
        }
    }

    public void fill_student(decimal grade, char classs)
    {
        try
        {
            DataTable dt_student = ialdc.get_all_students_of_class(classs, grade);
            if (dt_student.Rows.Count == 0)
            {
                lbl_class_check.Text = "No  Students";
                ddl_student.Items.Clear();
                lbl_student_check.Text = string.Empty;
            }
            else
            {
                lbl_class_check.Text = string.Empty;
                lbl_student_check.Text = string.Empty;
                ddl_student.DataSource = dt_student;
                ddl_student.DataMember = "NAME_INITIALS";
                ddl_student.DataValueField = "NAME_INITIALS";
                ddl_student.DataBind();
                ddl_student.Items.Insert(0, "Select Student");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! There are no students in the selected class')", true);
        }
    }
    public void fill_types()
    {
        try
        {
            DataTable dt_type = ialdc.getall_activity_type();
            if (dt_type.Rows.Count == 0)
            {
                lbl_class_check.Text = "No Activity Types";
                ddl_student.Items.Clear();
                lbl_activity_check.Text = string.Empty;
            }
            else
            {
                lbl_class_check.Text = string.Empty;
                lbl_student_check.Text = string.Empty;
                ddl_type.DataSource = dt_type;
                ddl_type.DataMember = "ACTIVITY_CODE";
                ddl_type.DataValueField = "ACTIVITY_CODE";
                ddl_type.DataBind();
                ddl_type.Items.Insert(0, "Select Type");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! There are no any activity type')", true);
        }
    }
    //fill the drop down list with activities
    public void fill_ddl_activities(string type)
    {
        try
        {
            DataTable dt_activity = ialdc.get_all_activity(type);
            if (dt_activity.Rows.Count == 0)
            {
                lbl_class_check.Text = "No Activity";
                ddl_student.Items.Clear();
                lbl_student_check.Text = string.Empty;
            }
            else
            {
                lbl_class_check.Text = string.Empty;
                lbl_student_check.Text = string.Empty;
                ddl_activity.DataSource = dt_activity;
                ddl_activity.DataMember = "ACTIVITYSUB_CODE";
                ddl_activity.DataValueField = "ACTIVITYSUB_CODE";
                ddl_activity.DataBind();
                ddl_activity.Items.Insert(0, "Select Activity");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! There are no any activites in the selected type')", true);
        }
    }

    protected void ddl_grade_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            ddl_class.Items.Clear();
            ddl_student.Items.Clear();
            ddl_type.Items.Clear();
            ddl_activity.Items.Clear();
            lbl_grade_check.Text = string.Empty;
            lbl_class_check.Text = string.Empty;
            lbl_student_check.Text = string.Empty;
            lbl_type_check.Text = string.Empty;
            lbl_activity_check.Text = string.Empty;
            if (ddl_grade.Text == "Select Grade")
            {
                lbl_grade_check.Text = "Invalid Grade";
                ddl_class.Items.Clear();
                ddl_student.Items.Clear();
            }
            else
            {
              
                fill_class(Convert.ToDecimal(ddl_grade.Text));
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! operation can not be fulfill')", true);


        }
    }
    protected void ddl_class_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddl_student.Items.Clear();
        ddl_type.Items.Clear();
        ddl_activity.Items.Clear();
        lbl_grade_check.Text = string.Empty;
        lbl_class_check.Text = string.Empty;
        lbl_student_check.Text = string.Empty;
        lbl_type_check.Text = string.Empty;
        lbl_activity_check.Text = string.Empty;
        if (ddl_class.Text == "Select Class")
        {
            lbl_class_check.Text = "Invalid Class";
            ddl_student.Items.Clear();
        }
        else
        {
            try
            {

                fill_student(Convert.ToDecimal(ddl_grade.Text), Convert.ToChar(ddl_class.Text));
            }
            catch (Exception d)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! operation can not be fulfill')", true);
                //Response.Redirect("http://localialdct:49178/SIMS_template3/Logged_Pages/ANon_Academic_Reports/LeavingCertificate.aspx");
            }

        }
    }
    protected void ddl_student_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddl_type.Items.Clear();
        ddl_activity.Items.Clear();
        lbl_grade_check.Text = string.Empty;
        lbl_class_check.Text = string.Empty;
        lbl_student_check.Text = string.Empty;
        lbl_type_check.Text = string.Empty;
        lbl_activity_check.Text = string.Empty;
        if (ddl_student.Text == "Select Student")
        {
            lbl_student_check.Text = "Invalid Student";
            ddl_student.Items.Clear();
        }
        else
        {
            try
            {

                fill_types();
            }
            catch (Exception d)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! operation can not be fulfill')", true);
                //Response.Redirect("http://localialdct:49178/SIMS_template3/Logged_Pages/ANon_Academic_Reports/LeavingCertificate.aspx");
            }

        }
    }

    protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddl_activity.Items.Clear();
        lbl_grade_check.Text = string.Empty;
        lbl_class_check.Text = string.Empty;
        lbl_student_check.Text = string.Empty;
        lbl_type_check.Text = string.Empty;
        lbl_activity_check.Text = string.Empty;
        if (ddl_type.Text == "Select Type")
        {
            lbl_type_check.Text = "Invalid Type";
            ddl_activity.Items.Clear();
        }
        else
        {
            try
            {

                fill_ddl_activities(ddl_type.Text);
            }
            catch (Exception d)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! operation can not be fulfill')", true);
                //Response.Redirect("http://localialdct:49178/SIMS_template3/Logged_Pages/ANon_Academic_Reports/LeavingCertificate.aspx");
            }

        }
    }
    protected void ddl_activity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_activity.Text == "Select Activity")
        {
            lbl_activity_check.Text = "Invalid Activity";
        }
        else
        {
            lbl_activity_check.Text = string.Empty;
        }
    }
    public void clear()
    {
        set_grade();
        ddl_class.Items.Clear();
        ddl_student.Items.Clear();
        ddl_type.Items.Clear();
        ddl_activity.Items.Clear();
        lbl_grade_check.Text = string.Empty;
        lbl_class_check.Text = string.Empty;
        lbl_student_check.Text = string.Empty;
        lbl_type_check.Text = string.Empty;
        lbl_activity_check.Text = string.Empty;
        txt_date.Text = string.Empty;
        txt_note.Text = string.Empty;
        txt_year.Text = string.Empty;

    }
    protected void txt_note_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn_insert_Click(object sender, EventArgs e)
    {

        try
        {
            string id = ialdc.get_student_id(ddl_student.Text);
            string member = set_regnum(id);

            if (ddl_grade.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, please select a grade')", true);
            }
            else if (ddl_class.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, please select a class')", true);
            }
            else if (ddl_student.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, please select a student')", true);
            }
            else if (ddl_type.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, please select a activty type')", true);
            }
            else if (ddl_activity.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, please select a activity')", true);
            }
            else if (txt_date.Text == string.Empty || checkdate(txt_date.Text) == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, Manually enterd date format is  incorrect')", true);
            }
            else if (member == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, Member ID doesnt created properly')", true);
            }
            else if (validate_year(txt_year.Text) == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!!, Activity year is not valid')", true);
            }
            else
            {
                if (ialdc.insert_student_activity(id, ddl_type.Text, ddl_activity.Text, Convert.ToDecimal(txt_year.Text), note(), member, txt_date.Text))
                {
                    //CustomException ex = new CustomException("Success !!!", "Activity Record Successfully Inserted");
                    //MessageBox messageBox = new MessageBox(ex, MessageBoxDefaultButtonEnum.OK);
                    //messageBox.Show(this);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Success!! Record inserted successfully')", true);
                    clear();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! operation can not be fulfill')", true);
                }
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!!, Please fill the fields properly')", true);
        }

    }
    public int checkdate(string date)
    {
        int a = 0;
        try
        {
            int year = Convert.ToInt16(date.Substring(6, 2));
            if (date.Length > 10 || date.Length < 8)
            {
                a = 0;
            }
            else if (!date.Contains('/'))
            {
                a = 0;
            }
            else if (Convert.ToInt16(date.Substring(0, 1)) > 12)
            {
                a = 0;
            }
            else if (year > 99)
            {
                a = 0;
            }

            else
            {
                a = 1;
            }
            return a;
        }
        catch
        {
            return a;
        }
    }
    protected void btn_clear_Click(object sender, EventArgs e)
    {
        clear();
    }

    public string set_regnum(string id)
    {
        string first = id.Substring(6, 4);
        string year = DateTime.Today.Year.ToString();
        string pre = "EA";
        return pre.Insert(2, year).Insert(6, first);
    }

    public string note()
    {
        if (txt_note.Text == string.Empty || txt_note.Text == null)
        {
            return "Null";
        }
        else
        {
            return txt_note.Text;
        }
    }

    public int validate_year(string year)
    {
        int b = 0;
        try
        {          
            if (year == "0")
            {
                return b;
            }
            else if (Convert.ToInt16(year) > 2050 || Convert.ToInt16(year) < 2000)
            {
                return b;
            }
            else
            {
                b = 1;
                return b;
            }
        }
        catch
        {
           b=0;
           return b;

        }
        

    }
}
