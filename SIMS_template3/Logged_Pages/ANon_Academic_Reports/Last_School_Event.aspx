﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/ReportTemplate_n.master" AutoEventWireup="true"
    CodeFile="Last_School_Event.aspx.cs" Inherits="Logged_Pages_AParents_Reports_Parent_Info"
    Title="Add to Photo Gallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <link href="../styles.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript">
    function checkDate(sender,args)
{
 if (sender._selectedDate > new Date())
            {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(); 
                // set the date back to the current date
sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
}
    </script>
    
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="Text1" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Small" ForeColor="White"></asp:Label>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="../Home.aspx">Home</a></li>
                    <li><a href="../AboutUs.aspx" class="active">About Us</a></li>
                    <li><a href="../ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>

                <div class="ReportBody">
                    <div class="ReportHeader">
                        School Special Events
                    </div>
                    <div class="ReportDesc">
                        Add School Special Events
                    </div>
                </div>
                <div class="FormHeadder">
                </div>
                <div class="FormBackgroundAddAppointment">
                    <asp:ToolkitScriptManager ID="ToolkitScriptManager_events" runat="server">
                    </asp:ToolkitScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    <div class="FormRow">
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label1" runat="server" class="RLable" Text="Event Name :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
<asp:TextBox ID="txt_eventname" runat="server" CssClass="FormTxtBox "  Width="185px" MaxLength="50"></asp:TextBox>
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txt_eventname" WatermarkText="Add event name">
                            </asp:TextBoxWatermarkExtender>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label2" runat="server" class="RLable" Text="Event Type :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:DropDownList class="RTxt" CssClass="FormTxtBox " ID="ddl_type" 
                                runat="server" Height="20px" Width="185px"
                     
                                AutoPostBack="true" onselectedindexchanged="ddl_type_SelectedIndexChanged" 
                                >

                                </asp:DropDownList>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label14" runat="server" class="RLable" Text="Catagory :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                           <asp:DropDownList class="RTxt" CssClass="FormTxtBox "  ID="ddl_catagoty" 
                                runat="server" Height="20px" Width="185px"
                                   AutoPostBack="true" onselectedindexchanged="ddl_catagoty_SelectedIndexChanged"
                                >
                              
                                </asp:DropDownList>
                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                    
                        <div class="FormRow">
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
                        
                        
                            <asp:Label ID="lbl" runat="server" class="RLable" ForeColor="Red" 
                                Width="175px"></asp:Label>
                        
                        
                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
                            <asp:Label ID="lbl_class_check" runat="server" class="RLable" ForeColor="Red" 
                                Width="175px"></asp:Label>
                        </div>
                        <div class="fieldsCaptionCell">                       
                        </div>
                        <div class="fieldsElementCell">
                            <asp:Label ID="lbl_student_check" runat="server" class="RLable" ForeColor="Red"></asp:Label>
                                
                        </div>
                    </div>

                    <div class="paraSpace">
                    </div>
                                            <div class="FormRow">
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label4" runat="server" class="RLable" Text="Organizer :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
<asp:TextBox ID="txt_organizer" runat="server" CssClass="FormTxtBox "  Width="185px" MaxLength="20"></asp:TextBox>
 
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txt_organizer" WatermarkText="Add Organizer">
                            </asp:TextBoxWatermarkExtender>
                        </div>
                        <div class="fieldsCaptionCell">
                            <asp:Label ID="Label5" runat="server" class="RLable" Text="Held Date :"></asp:Label>
                        </div>
                        <div class="fieldsElementCell">
                            <asp:TextBox ID="txt_date" runat="server" Class="FormTxtBox " 
                                Font-Names="Arial" Width="165px" MaxLength="10"></asp:TextBox>
                            <asp:TextBoxWatermarkExtender ID="wme_date" runat="server" TargetControlID="txt_date"
                                WatermarkText="Select Date">
                            </asp:TextBoxWatermarkExtender>
                            <asp:CalendarExtender ID="ce_warning" runat="server" TargetControlID="txt_date" PopupButtonID="ibtn_warning" OnClientDateSelectionChanged="checkDate">
                            </asp:CalendarExtender>
                            <asp:ImageButton ID="ibtn_warning" runat="server" Height="16px" ImageUrl="~/images/Calendar_scheduleHS.png"
                                Width="16px" />
                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">
                                
                        </div>
                    </div>
                                        <div class="paraSpace">
                    </div>
                                                              <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">
                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">
                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">
                                
                        </div>
                    </div>
                                                            <div class="paraSpace">
                    </div>

                                        <div class="field_3_Row162app">
                        <div class="fieldsCaptionCell162app">
                            <asp:Label ID="Label3" runat="server" class="RLable" Text="Description :"></asp:Label>
                        </div>
                        <asp:TextBox ID="txt_description" Class="FormTxtBox " runat="server" Height="160px" TextMode="MultiLine"
                            Width="669px" MaxLength="500" ></asp:TextBox>
                    </div>
                    <div class="paraSpace">
                    </div>
                                                              <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">
                                
                        </div>
                    </div>
                                                            <div class="paraSpace">
                    </div>
                                        <div class="field_3_Row162app">
                        <div class="fieldsCaptionCell162app">
                            <asp:Label ID="Label15" runat="server" class="RLable" Text="Faced Issues :"></asp:Label>
                        </div>
                        <asp:TextBox ID="txt_issues" Class="FormTxtBox " runat="server" Height="160px" TextMode="MultiLine"
                            Width="669px" MaxLength="500" ></asp:TextBox>
                    </div>
                                        <div class="paraSpace">
                    </div>
                                        <div class="paraSpace">
                    </div>
                                          <div class="FormRow">
                        <div class="fieldsCaptionCell">
                          
                        </div>
                        <div class="fieldsElementCell">
                                                    <asp:Button ID="btn_insert" runat="server" Text="Insert" Height="30px" Width="142px"
                               CssClass="buttonText" ValidationGroup="submit" onclick="btn_insert_Click" />
                        </div>
                        <div class="fieldsCaptionCell">
                           
                        </div>
                        <div class="fieldsElementCell">
                                                    <asp:Button ID="btn_clear" runat="server" Height="30px" Width="142px" Text="Clear"
                                CssClass="buttonText" onclick="btn_clear_Click"  />

                        </div>
                        <div class="fieldsCaptionCell">
                            
                        </div>
                        <div class="fieldsElementCell">

                                
                        </div>
                    </div>
                    <div class="paraSpace">
                    </div>
                                        <div class="field_3_Row">
                        <div class="fieldsCaptionCell">
                        </div>
                        <div class="fieldsElementCell">

                        </div>
                        <div class="fieldsCaptionCell">

                        </div>
                        <div class="fieldsCaptionCell">
                        </div>
                    </div>
                          <div class="RepPanelauto">

                    </div>
                     <div class="paraSpace">
                    </div>
                    </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddl_catagoty" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddl_type" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btn_clear" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btn_insert" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

                        







                </div>
                <div class="FormFooter">
                </div>

    </div>
</asp:Content>
