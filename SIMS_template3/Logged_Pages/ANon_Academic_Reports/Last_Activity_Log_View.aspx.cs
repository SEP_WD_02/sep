﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;

public partial class Logged_Pages_AParents_Reports_Parent_Info : System.Web.UI.Page
{
    /// <summary>
    /// Author - E.A Heshan Sandeepa IT 10 1598 04
    /// 2012-08-16
    /// this is the comman variable to access the database
    /// </summary>
    Last_Activity_Edit lae = new Last_Activity_Edit();
    /// <summary>
    /// when page loading all the records in the table would be there
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                fill();
            }
            catch
            {
                 ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Page can not be loaded')", true);
            }
        }
    }

    public void fill()
    {
        try
        {

            gv_UD_activity.DataSource = lae.get_all();
            gv_UD_activity.DataBind();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Student  activity detais can not be loaded')", true);

        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (txt_search.Text == string.Empty || txt_search.Text == null)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Warning!! Insert Search Key First')", true);
        }
        else
        {
            try
            {
                gv_UD_activity.DataSource = lae.search_results(txt_search.Text.Trim());
                gv_UD_activity.DataBind();
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "alert('Error!! Incorrect Search Key')", true);
            }
        }
    }
    protected void gv_UD_activity_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_UD_activity.PageIndex = e.NewPageIndex;
        fill();
    }
}
