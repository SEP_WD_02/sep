﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/MainMaster.master" AutoEventWireup="true"
    CodeFile="Academic_Home.aspx.cs" Inherits="Academic_Home" Title="Academic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="txtUser" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Medium" ForeColor="White"></asp:Label>
                <br />
                <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Arial" Font-Size="Small"
                    ForeColor="Yellow" NavigateUrl="~/Common_Home.aspx"> Logout</asp:HyperLink>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="Home.aspx">Home</a></li>
                    <li><a href="AboutUs.aspx">About Us</a></li>
                    <li><a href="ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <!--///////////////////////////// new frame work implimentation /////////////////////////         -->
        <!--Navigator -->
            <div style="background-color:White; width:958px; height:35px; padding:5px;">
                <div style="height:32px; width:954px; border-color:#0280EC; border-width:1px; border-style:solid; padding:1px;">
                <div class="NaviTabCommon">
                    <asp:HyperLink ID="HyperLink7" runat="server" 
                        NavigateUrl="~/Logged_Pages/Home.aspx" CssClass="HyperLink">Main Home</asp:HyperLink>
                </div>
                <div class="NaviTabFinal">
                    <asp:HyperLink ID="HyperLink8" runat="server" 
                        NavigateUrl="~/Logged_Pages/Academic_Home.aspx" CssClass="HyperLink">Academic</asp:HyperLink>
                </div>
                </div>
            </div>
            <!--Navigator -->
        <div style="background-color:White; height:5px;"></div>
        <div id="MHedding">
            Academic Home Page         </div>
        <div class="MHeaderDisc">
            You can generate following reports.
        </div>
        <div class="MCtrlPanel">
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel1" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbProgressReports" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbProgressReports_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Progress Reports
                    </div>
                    <div class="MDesctxt">
                        Individual progress report for students
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel2" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbPrizeWinners" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbPrizeWinners_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Prize Winners
                    </div>
                    <div class="MDesctxt">
                        School prize giving by performance of term test marks
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel3" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbBestPerformance" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="btnBestPerformance_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Best Performance
                    </div>
                    <div class="MDesctxt">
                        The best performance in government examinations.
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel4" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbGenAcaProgress" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbGenAcaProgress_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        General Examination Progress
                    </div>
                    <div class="MDesctxt">
                        Generate Examination Progressreports automaticaly
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel5" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbExamResultSheets" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbExamResultSheets_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Exammination Result Details</div>
                    <div class="MDesctxt">
                        Scholarship, Ordinary level &amp; Advanced level result details
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel6" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbEvent" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbEvent_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Student&#39;s Photo Gallery
                    </div>
                    <div class="MDesctxt">
                        Watch the School Student&#39;s Photo Gallery with the photoes shot at school events.
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel7" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbExamResultAnalysis" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbExamResultAnalysis_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Examination Result Analysis
                    </div>
                    <div class="MDesctxt">
                        Scholarship, Ordinary level &amp; Advanced level result for entire school year wise
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel8" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbAddMedical" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbAddMedical_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Add Medical Details
                    </div>
                    <div class="MDesctxt">
                        Add Government Examination Medical Details for the Students.
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel9" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbViewMedical" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbViewMedical_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        View Medical Details
                    </div>
                    <div class="MDesctxt">
                        View Government Examination Medical Details of the Students.
                    </div>
                </div>
            </asp:Panel>
        </div>
        <!--///////////////////////////// new frame work implimentation /////////////////////////         -->
        <!-- Extra features Start-->
        <div style="height: 17px; width: 968px; background-image: url('images/LowerSlider.jpg');">
        </div>
    <div style="height: 200px; width: 968px; background-image: url('images/register.jpg');">
        <div style="float: left; width: 220px; height: 195px; padding-left: 50px;">
            <asp:Panel ID="Panel10" runat="server" Height="195px" Width="255px">
                <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" 
                    LineImagesFolder="~/TreeLineImages">
                    <ParentNodeStyle Font-Bold="False" />
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                        VerticalPadding="0px" ForeColor="#5555DD" />
                    <Nodes>
                        <asp:TreeNode Expanded="True" NavigateUrl="~/Logged_Pages/Home.aspx" Text="Home"
                            Value="Home">
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/Academic_Home.aspx" Text="Academic Page"
                                Value="Academic Page"></asp:TreeNode>
                            <asp:TreeNode Text="Non-Academic Page" Value="Non-Academic Page"></asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/Parents_Home.aspx" Text="Parents Page"
                                Value="Parents Page"></asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/StudentHome.aspx" Text="Student Page" Value="Student Page">
                            </asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Logged_Pages/AboutUs.aspx" Text="About us" Value="About us">
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Logged_Pages/ContactUs.aspx" Text="Contact us" Value="Contact us">
                        </asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/Common_Home.aspx" Text="Logout" Value="Logout"></asp:TreeNode>
                    </Nodes>
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Blue" HorizontalPadding="5px"
                        NodeSpacing="0px" VerticalPadding="0px" CssClass="HyperLink" />
                </asp:TreeView>
            </asp:Panel>
        </div>
        <!--Security declaration -->
        <div style="float: left; width: 435px; height: 195px; padding-left: 20px;">
            <div id="SecurityDiv" style="width: 270px; height: 195px; padding-left: 155px;">
                <ui>
                    <li><b>Authenticity: </b>Website provides you unique username &amp; password. Also you 
                        can change your current password frequently. It will prevent your account from 
                        unauthorized access. Visit your profile page for more details..<asp:HyperLink 
                            ID="HyperLink6" runat="server" Font-Names="Arial" Font-Size="11px" 
                            Font-Underline="True" ForeColor="#FF6600" 
                            NavigateUrl="~/Logged_Pages/Profile.aspx">Visit Profile</asp:HyperLink></li>
                            
                    <li><b>Authorization: </b>The web server keeps few user levels to establish 
                        confidentiality between each user’s duties. So that users are independent to do 
                        their own work..</li>
                    </ui>
            </div>
        </div>
        <div style="float: left; width: 230px; height: 195px; padding-left: 5px;">
            <div style="float: left; width: 220px; height: 20px; text-align: center; font-size: 17px;
                padding: 5px; font-weight: bold;">
                Useful Resources
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/UGC.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.ugc.ac.lk/" CssClass="HyperLink">Sri Lanka 
                    University Grand Commission</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/NIE.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.nie.lk/" CssClass="HyperLink">Sri Lanka National 
                    Institute of Education</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/SED.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="12px" 
                        NavigateUrl="http://www.doenets.lk/exam/" CssClass="HyperLink">Department of 
                    Examination Sri Lanka</asp:HyperLink>
                </div>
            </div>
            <div style="float: left; width: 220px; height: 40px;">
                <div style="float: left; width: 40px; height: 40px; background-image: url('images/FB.jpg');">
                </div>
                <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                    color: #000066; text-align: left;">
                    <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="12px" 
                        NavigateUrl="https://www.facebook.com/" CssClass="HyperLink">School Info 
                    Management System - Face Book</asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
    <!-- Extra features Ends-->
        <div class="con_bot">
        </div>        
        <div id="footer">
            <p>
                All Rights Reserved</p>
            <!-- Please DO NOT remove the following notice -->
            <p>
                Designed by Sri Lanka Institute of Information Technology (Software Engineering 
                Batch – week day group No: 2)</p>
            <!-- end of copyright notice-->
        </div>
    </div>
</asp:Content>
