﻿<%@ Page Language="C#" MasterPageFile="~/Logged_Pages/MainMaster.master" AutoEventWireup="true"
    CodeFile="StudentHome.aspx.cs" Inherits="StudentHome" Title="Students Home Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo2">
                <br />
                <br />
                <br />
                <asp:Label ID="txtUser" runat="server" Text="Session Name" Font-Bold="False" Font-Names="Arial"
                    Font-Size="Medium" ForeColor="White"></asp:Label>
                <br />
                <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Arial" Font-Size="Small"
                    ForeColor="Yellow" NavigateUrl="~/Common_Home.aspx">Logout</asp:HyperLink>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="Home.aspx">Home</a></li>
                    <li><a href="AboutUs.aspx">About Us</a></li>
                    <li><a href="ContactUs.aspx">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <!--Navigator -->
            <div style="background-color:White; width:958px; height:35px; padding:5px;">
                <div style="height:32px; width:954px; border-color:#0280EC; border-width:1px; border-style:solid; padding:1px;">
                <div class="NaviTabCommon">
                    <asp:HyperLink ID="HyperLink7" runat="server" 
                        NavigateUrl="~/Logged_Pages/Home.aspx" CssClass="HyperLink">Main Home</asp:HyperLink>
                </div>
                <div class="NaviTabFinal">
                    <asp:HyperLink ID="HyperLink8" runat="server" 
                        NavigateUrl="~/Logged_Pages/StudentHome.aspx" CssClass="HyperLink">Students</asp:HyperLink>
                </div>
                </div>
            </div>
         <!--Navigator -->
        <!--///////////////////////////// new frame work implimentation /////////////////////////         -->
        <div style="background-color: White; height: 5px;">
        </div>
        <div id="MHedding">
            Student Home Page         </div>
        <div class="MHeaderDisc">
            You can generate following reports.
        </div>
        <div class="MCtrlPanel">
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel1" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbTotalStudents" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbTotalStudents_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Total Students</div>
                    <div class="MDesctxt">
                        Number of Students Categorywise</div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel2" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbStudentDetails" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbStudentDetails_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        View Student Details
                    </div>
                    <div class="MDesctxt">
                        Enter student details and view student details&nbsp;
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel3" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbClassdetails" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbClassdetails_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Class Details Report
                    </div>
                    <div class="MDesctxt">
                        Report of students in class according to different criterias.
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel4" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="imbEnrolled" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg"
                            OnClick="imbEnrolled_Click" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Student Intake Counter
                    </div>
                    <div class="MDesctxt">
                        Report for total number of students registered per year
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel5" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Student Leaving Details
                    </div>
                    <div class="MDesctxt">
                        Report for total number of students left the school per year</div>
                </div>
            </asp:Panel>
            <asp:Panel class="Boundrypannel" runat="server" ID="Panel6" Width="282px" CssClass="Boundrypannel">
                <div class="Mlinker">
                    <div class="Mimg">
                        <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Logged_Pages/userImages/Untitled-2.jpg" />
                        &nbsp;</div>
                    <div class="Mtxt">
                        Eligible for Prefects
                    </div>
                    <div class="MDesctxt">
                        Provide details for the selection of school prefects</div>
                </div>
            </asp:Panel>
        </div>
        <!--///////////////////////////// new frame work implimentation /////////////////////////         -->
        <!-- Extra features Start-->
        <div style="height: 17px; width: 968px; background-image: url('images/LowerSlider.jpg');">
        </div>
        <div style="height: 200px; width: 968px; background-image: url('images/register.jpg');">
            <div style="float: left; width: 220px; height: 195px; padding-left: 50px;">
                <asp:Panel ID="Panel7" runat="server" Height="195px" Width="255px">
                    <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" 
                        LineImagesFolder="~/TreeLineImages">
                        <ParentNodeStyle Font-Bold="False" />
                        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                        <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                            VerticalPadding="0px" ForeColor="#5555DD" />
                        <Nodes>
                            <asp:TreeNode Expanded="True" NavigateUrl="~/Logged_Pages/Home.aspx" Text="Home"
                                Value="Home">
                                <asp:TreeNode NavigateUrl="~/Logged_Pages/Academic_Home.aspx" Text="Academic Page"
                                    Value="Academic Page"></asp:TreeNode>
                                <asp:TreeNode Text="Non-Academic Page" Value="Non-Academic Page"></asp:TreeNode>
                                <asp:TreeNode NavigateUrl="~/Logged_Pages/Parents_Home.aspx" Text="Parents Page"
                                    Value="Parents Page"></asp:TreeNode>
                                <asp:TreeNode NavigateUrl="~/Logged_Pages/StudentHome.aspx" Text="Student Page" Value="Student Page">
                                </asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/AboutUs.aspx" Text="About us" Value="About us">
                            </asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Logged_Pages/ContactUs.aspx" Text="Contact us" Value="Contact us">
                            </asp:TreeNode>
                            <asp:TreeNode NavigateUrl="~/Common_Home.aspx" Text="Logout" Value="Logout"></asp:TreeNode>
                        </Nodes>
                        <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Blue" HorizontalPadding="5px"
                            NodeSpacing="0px" VerticalPadding="0px" CssClass="HyperLink" />
                    </asp:TreeView>
                </asp:Panel>
            </div>
            <!--Security declaration -->
            <div style="float: left; width: 435px; height: 195px; padding-left: 20px;">
                <div id="SecurityDiv" style="width: 270px; height: 195px; padding-left: 155px;">
                    <ui>
                    <li><b>Authenticity: </b>Website provides you unique username &amp; password. Also you 
                        can change your current password frequently. It will prevent your account from 
                        unauthorized access. Visit your profile page for more details..<asp:HyperLink 
                            ID="HyperLink6" runat="server" Font-Names="Arial" Font-Size="11px" 
                            Font-Underline="True" ForeColor="#FF6600" 
                            NavigateUrl="~/Logged_Pages/Profile.aspx">Visit Profile</asp:HyperLink></li>
                            
                    <li><b>Authorization: </b>The web server keeps few user levels to establish 
                        confidentiality between each user’s duties. So that users are independent to do 
                        their own work..</li>
                    </ui>
                </div>
            </div>
            <div style="float: left; width: 230px; height: 195px; padding-left: 5px;">
                <div style="float: left; width: 220px; height: 20px; text-align: center; font-size: 17px;
                    padding: 5px; font-weight: bold;">
                    Useful Resources
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/UGC.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.ugc.ac.lk/" CssClass="HyperLink">Sri Lanka 
                        University Grand Commission</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/NIE.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.nie.lk/" CssClass="HyperLink">Sri Lanka National 
                        Institute of Education</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/SED.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.doenets.lk/exam/" CssClass="HyperLink">Department of 
                        Examination Sri Lanka</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/FB.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="12px" 
                            NavigateUrl="https://www.facebook.com/" CssClass="HyperLink">School Info 
                        Management System - Face Book</asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <!-- Extra features Ends-->
        <div class="con_bot">
        </div>
        <div id="footer">
            <p>
                All Rights Reserved</p>
            <!-- Please DO NOT remove the following notice -->
            <p>
                Designed by Sri Lanka Institute of Information Technology (Software Engineering 
                Batch – week day group No: 2)</p>
            <!-- end of copyright notice-->
        </div>
    </div>
</asp:Content>
