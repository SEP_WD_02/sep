﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="Common_AboutUs.aspx.cs" Inherits="Common_AboutUs" Title="About us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
        <div id="wrap">
            <div id="header">
                <div id="logo">
                    <asp:ScriptManager ID="AtbUsScrMng" runat="server">
                    </asp:ScriptManager>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUser"
                            ErrorMessage="Enter Correct Username" ForeColor="White"></asp:RequiredFieldValidator>
                    <asp:UpdatePanel ID="AbtusUpPnl" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtUser" runat="server" BorderStyle="None" Font-Size="12px"></asp:TextBox>
                            <br />
                            <asp:TextBox ID="txtPass" runat="server" BorderStyle="None" TextMode="Password" 
                                Font-Size="12px"></asp:TextBox>
                            <br />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSignin" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    
                    <asp:ImageButton ID="btnSignin" runat="server" ImageAlign="Middle" ImageUrl="~/images/sign in.png"
                        OnClick="btnSignin_Click" />
                    &nbsp;<br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Correct Password"
                    ControlToValidate="txtPass" ForeColor="White"></asp:RequiredFieldValidator>
                        
                        
                </div>
                <div id="menu">
                    <ul>
                        <li><a href="Common_Home.aspx">Home</a></li>
                        <li><a href="Common_AboutUs.aspx" class="active">About Us</a></li>
                        <li><a href="Common_ContactUs.aspx">Contact Us</a></li>
                    </ul>
                </div>
            </div>
             <!--Navigator -->
            <div style="background-color:White; width:958px; height:35px; padding:5px;">
                <div style="height:32px; width:954px; border-color:#0280EC; border-width:1px; border-style:solid; padding:1px;">
                <div class="NaviTabFinal">
                    <asp:HyperLink ID="HyperLink7" runat="server" 
                        NavigateUrl="~/Logged_Pages/Home.aspx" CssClass="HyperLink"> About us</asp:HyperLink>
                </div>
                </div>
            </div>
            <!--Navigator -->
            <div id="prevX">
               
                <asp:Image ID="Image1" runat="server" Height="294px" 
                    ImageUrl="~/images/about_us_image.jpg" Width="950px" />
               
            </div>

            <div id="content">
                <div class="about">
                    <br />
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Medium" 
                        ForeColor="#0099CC" Text="History"></asp:Label>
                </div>
                <div id="aboutusTxt" 
                    style="clear: both; font-size: 12px; font-family: Arial; color: #000000; text-align: justify;"><br />
                    ABC College was established in 1956 in the aspiration of endowing well educated 
                    citizens to the nation. Today, ABC is a national school recognized by the 
                    Ministry of Education and an independent, non-profit, educational institute 
                    which offers education from grade 1 to grade 13. There are more than 3,500 
                    students studying in the school at the present.<br /><br /> 
                    The school includes three distinct divisions: Primary School (grade one through 
                    grade five), Middle School (grades six through eleven), and High School (grades 
                    twelve through thirteen). The professional staff is around 300 with nearly 60% 
                    holding Bsc. degrees or above.<br /><br /> ABC College provides a safe and 
                    pleasant learning environment in one of the finest school facilities in the 
                    country. ABC students consistently rank higher on academic and non academic 
                    achievements. Class sizes emphasize educationally sound student-teacher ratios. 
                    Student/teacher ratios across all of the divisions reflect the ABC Colleges 
                    commitment to ensuring a quality education and a high level of pastoral care for 
                    all of our students.<br /></div>
            </div>
            <div style="height: 17px; width: 968px; background-image: url('images/LowerSliderX.jpg');"></div>
            <div style="height:200px; width: 968px; background-image: url('images/register.jpg');">
            <div style="float:left; width:560px; height:195px; padding-left:160px; ">
                <div style="float:left; width:560px; height:40px;"><br /><br /><hr /></div>
                <div style="padding:5px; float:left; width:550px; height:60px; font-family: Arial; font-size: 12px; color: #000000; text-align:justify;">
                    Web sites are critical areas which can be affected by unauthorized users. Cause 
                    of that security issue, this site never allows making accounts through online. 
                    The web site can be accessed by only Educational Department and related schools.  
                </div>
                <div style="float:left; width:540px; height:30px; font-family: Arial; font-size: 14px; color: #003399; text-align:justify; padding-left:20px; padding-top:10px;">
                    1). Accounts for staff members will be issued by IT Department of your office 
                    place. 
                </div>
                 <div style="float:left; width:540px; height:30px; font-family: Arial; font-size: 14px; color: #339933; text-align:justify; padding-left:20px; padding-top:10px;">
                     2). Accounts for teachers &amp; parents will be issued by IT Division of your 
                     school.
                </div>
            </div>
            <div style="float:left; width:230px; height:195px; padding-left:5px;">
                <div style="float:left; width:220px; height:20px; text-align:center; font-size:17px; padding:5px; font-weight:bold;">
                    Useful Resources 
                 </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/UGC.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;" class=" ">
                        <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.ugc.ac.lk/" CssClass="HyperLink" Font-Names="Arial">Sri 
                        Lanka University Grand Commission</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/NIE.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.nie.lk/" CssClass="HyperLink">Sri Lanka National 
                        Institute of Education</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/SED.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.doenets.lk/exam/" CssClass="HyperLink">Department of 
                        Examination Sri Lanka</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/FB.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;"> 
                        <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="12px" 
                            NavigateUrl="https://www.facebook.com/" CssClass="HyperLink">School Info 
                        Management System - Face Book</asp:HyperLink>
                    </div>
                </div>
            </div>
        
        </div>
            <div class="con_bot"></div>
            <div id="footer">
                 <p>
                     All Rights Reserved</p>
            <!-- Please DO NOT remove the following notice -->
            <p>
                Designed by Sri Lanka Institute of Information Technology (Software Engineering 
                Batch – week day group No: 2)</p>
            <!-- end of copyright notice-->
            </div>
        </div>
        
</asp:Content>


