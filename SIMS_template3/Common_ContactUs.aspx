﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeFile="Common_ContactUs.aspx.cs" Inherits="Common_AboutUs" Title="Contact us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="wrap">
        <div id="header">
            <div id="logo">
                <asp:ScriptManager ID="ConUsScrptMng" runat="server">
                </asp:ScriptManager>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUser"
                    ErrorMessage="Enter Correct Username" ForeColor="White" ValidationGroup="signin"></asp:RequiredFieldValidator>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtUser" runat="server" BorderStyle="None" 
                            ValidationGroup="signin" Font-Size="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="txtPass" runat="server" BorderStyle="None" TextMode="Password" 
                            ValidationGroup="signin" Font-Size="12px"></asp:TextBox>
                        <br />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSignin" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ImageButton ID="btnSignin" runat="server" ImageAlign="Middle" ImageUrl="~/images/sign in.png"
                    OnClick="btnSignin_Click" ValidationGroup="signin" />
                &nbsp;<br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Correct Password"
                    ControlToValidate="txtPass" ForeColor="White" ValidationGroup="signin"></asp:RequiredFieldValidator>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="Common_Home.aspx">Home</a></li>
                    <li><a href="Common_AboutUs.aspx">About Us</a></li>
                    <li><a href="Common_ContactUs.aspx" class="active">Contact Us</a></li>
                </ul>
            </div>
        </div>
         <!--Navigator -->
            <div style="background-color:White; width:958px; height:35px; padding:5px;">
                <div style="height:32px; width:954px; border-color:#0280EC; border-width:1px; border-style:solid; padding:1px;">
                <div class="NaviTabFinal">
                    <asp:HyperLink ID="HyperLink7" runat="server" 
                        NavigateUrl="~/Common_ContactUs.aspx" CssClass="HyperLink"> Contact us</asp:HyperLink>
                </div>
                </div>
            </div>
            <!--Navigator -->
        <div id="prevX">
            <asp:Image ID="Image1" runat="server" Height="297px" ImageUrl="~/images/contactus.jpg"
                Width="950px" />
        </div>
        <div id="content">
            <div class="proSubtitle">
                Contact Form                 <div class="TextDiv">
                    </br> Contact us for your future preferences.
                </div>
                </br>
                <div class="proRow">
                    <div class="RowcolLft">
                        Your Name</div>
                    <div class="RowcolRte">
                        :<asp:TextBox ID="txtName" runat="server" Height="20px" Width="580px" CssClass="commonTextBox"></asp:TextBox>
                    </div>
                    <div style="float: left; height: 30px; width: 120px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Incompleted"
                            Width="100px" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </br>
                <div class="proRow">
                    <div class="RowcolLft">
                        E-mail Address</div>
                    <div class="RowcolRte">
                        :<asp:TextBox ID="txtMail" runat="server" Height="20px" Width="580px" CssClass="commonTextBox"></asp:TextBox>
                    </div>
                    <div style="float: left; height: 30px; width: 120px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Incompleted"
                            Width="100px" ControlToValidate="txtMail"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </br>
                <div class="proRow">
                    <div class="RowcolLft">
                        Subject</div>
                    <div class="RowcolRte">
                        :<asp:TextBox ID="txtSubject" runat="server" Height="20px" Width="580px" CssClass="commonTextBox"></asp:TextBox>
                    </div>
                    <div style="float: left; height: 30px; width: 120px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="* Incompleted"
                            Width="100px" ControlToValidate="txtSubject"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </br>
                <div class="proRow">
                    <div class="RowcolLft">
                        Message</div>
                    <div class="RowcolRte">
                        :<asp:TextBox ID="txtMessage" runat="server" Height="20px" Width="580px" CssClass="commonTextBox"></asp:TextBox>
                    </div>
                    <div style="float: left; height: 30px; width: 120px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Incompleted"
                            Width="100px" ControlToValidate="txtMessage"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </br>
                <div class="proRow">
                    <div class="RowcolLft">
                        <asp:Button ID="btnSubmit" runat="server" Height="25px" Text="Submit" Width="139px"
                            OnClick="btnSubmit_Click" CssClass="buttonText" />
                    </div>
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </div>
                </br>
            </div>
        </div>
        <div style="height: 200px; width: 968px; background-image: url('images/register.jpg');">
            <div style="float: left; width: 560px; height: 195px; padding-left: 160px;">
                <div style="float: left; width: 560px; height: 40px;">
                    <br />
                    <br />
                    <hr />
                </div>
                <div style="padding: 5px; float: left; width: 550px; height: 60px; font-family: Arial;
                    font-size: 14px; color: #000000; text-align: justify;">
                    Web sites are critical areas which can be affected by unauthorized users. Cause 
                    of that security issue, this site never allows making accounts through online. 
                    The web site can be accessed by only Educational Department and related schools.
                </div>
                <div style="float: left; width: 540px; height: 30px; font-family: Arial; font-size: 14px;
                    color: #003399; text-align: justify; padding-left: 20px; padding-top: 10px;">
                    1). Accounts for staff members will be issued by IT Department of your office 
                    place.
                </div>
                <div style="float: left; width: 540px; height: 30px; font-family: Arial; font-size: 14px;
                    color: #339933; text-align: justify; padding-left: 20px; padding-top: 10px;">
                    2). Accounts for teachers &amp; parents will be issued by IT Division of your 
                    school.
                </div>
            </div>
            <div style="float: left; width: 230px; height: 195px; padding-left: 5px;">
                <div style="float: left; width: 220px; height: 20px; text-align: center; font-size: 17px;
                    padding: 5px; font-weight: bold;">
                    Useful Resources
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/UGC.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.ugc.ac.lk/" CssClass="HyperLink">Sri Lanka 
                        University Grand Commission</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/NIE.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.nie.lk/" CssClass="HyperLink">Sri Lanka National 
                        Institute of Education</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/SED.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="12px" 
                            NavigateUrl="http://www.doenets.lk/exam/" CssClass="HyperLink">Department of 
                        Examination Sri Lanka</asp:HyperLink>
                    </div>
                </div>
                <div style="float: left; width: 220px; height: 40px;">
                    <div style="float: left; width: 40px; height: 40px; background-image: url('images/FB.jpg');">
                    </div>
                    <div style="float: left; width: 180px; height: 40px; font-family: Arial; font-size: 12px;
                        color: #000066; text-align: left;">
                        <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="12px" 
                            NavigateUrl="https://www.facebook.com/" CssClass="HyperLink">School Info 
                        Management System - Face Book</asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="con_bot">
        </div>
        <div id="footer">
            <p>
                All Rights Reserved</p>
            <!-- Please DO NOT remove the following notice -->
            <p>
                Designed by Sri Lanka Institute of Information Technology (Software Engineering 
                Batch – week day group No: 2)</p>
            <!-- end of copyright notice-->
        </div>
    </div>
</asp:Content>
