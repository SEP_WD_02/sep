﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;

/// <summary>
/// Summary description for Last_Warning_View
/// </summary>
public class Last_Warning_View
{
    //define static connection string
    static string path = "Dsn=nsis;uid=root;pwd=123";
    static OdbcConnection con;
    public Last_Warning_View()
    {
        //
        // TODO: Add constructor logic here
        //
        //create new connection
        con = new OdbcConnection(path);
    }
    public OdbcConnection getconnection()
    {
        //return the created connection
        return con;
    }
    //select all the recorsd that satisfy id
    public DataTable search_results(string searchkey)
    {
        try
        {
            //create data table object
            DataTable dt_select_record;
            //set up new connection
            OdbcConnection con = getconnection();
            //open the setup coneection
            con.Open();
            //cretae datapter
            OdbcDataAdapter adapter = new OdbcDataAdapter("select *  from student_warning where ADMISSION_NO like '%" + searchkey + "%'", con);
            dt_select_record = new DataTable();
            //fill the datatable
            adapter.Fill(dt_select_record);
            //close the connection
            con.Close();
            return dt_select_record;
        }
        catch
        {
            return null;

        }

    }
    //get all the records of the table
    public DataTable databind()
    {
        try
        {
            //create data table object
            DataTable dt_select_record;
            //set up new connection
            OdbcConnection con = getconnection();
            //open the setup coneection
            con.Open();
            //cretae datapter
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from student_warning ", con);
            dt_select_record = new DataTable();
            //fill the datatable
            adapter.Fill(dt_select_record);
            //close the connection
            con.Close();
            return dt_select_record;
        }
        catch
        {
            return null;

        }
    }


}
