﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;

/// <summary>
/// Summary description for Last_Event
/// </summary>
public class Last_Event
{
    //define static connection string
    static string path = "Dsn=nsis;uid=root;pwd=123";
    static OdbcConnection con;
    public Last_Event()
    {
        con = new OdbcConnection(path);
    }

    public OdbcConnection getconnection()
    {
        //return the created connection
        return con;
    }

    //get all master events
    public DataTable get_event_master()
    {
        try
        {
            DataTable dt_master;
            //create new connection with the databse
            OdbcConnection conn = getconnection();
            //open the created connection
            conn.Open();
            //declare dataadpter variable and hold the values 
            OdbcDataAdapter adapter = new OdbcDataAdapter("select distinct emast from event_mast ", conn);
            //create new data tbale object
            dt_master = new DataTable();
            //fill the data table object through data adapter
            adapter.Fill(dt_master);
            //close the open clonnection
            conn.Close();
            //return the filled data table object
            return dt_master;
        }
        catch
        {
            return null;
        }
    }
    //get all sub events
    public DataTable get_subevents(string mast)
    {
        try
        {
            DataTable dt_sub;
            //create new connection with the databse
            OdbcConnection conn = getconnection();
            //open the created connection
            conn.Open();
            //declare dataadpter variable and hold the values 
            OdbcDataAdapter adapter = new OdbcDataAdapter("select distinct esub from event_mast where emast='" + mast + "'", conn);
            //create new data tbale object
            dt_sub = new DataTable();
            //fill the data table object through data adapter
            adapter.Fill(dt_sub);
            //close the open clonnection
            conn.Close();
            //return the filled data table object
            return dt_sub;

        }
        catch
        {
            return null;
        }
    }

    //insert record 
    public bool insert_record(string id, string type, string catagory, string name, string date, string des, string organizer, string issue)
    {
        bool stat = false;
        try
        {
            //create new connection with the databse
            OdbcConnection con = getconnection();
            //open the conection
            con.Open();
            //set command text
            OdbcCommand cmd = new OdbcCommand("insert into school_events values('" + id + "','" + type + "','" + catagory + "','" + name + "','" + date + "','" + des + "','" + organizer + "','" + issue + "')", con);
            //execute quary
            cmd.ExecuteNonQuery();
            //set statuss as true
            stat = true;
            //close the connection
            con.Close();
        }
        catch (Exception ss)
        {
            string sss = ss.ToString();

        }
        return stat;
    }
}
