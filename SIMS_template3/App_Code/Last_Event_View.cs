﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Last_Event_View
/// </summary>
public class Last_Event_View
{
    //define static connection string
    static string path = "Dsn=nsis;uid=root;pwd=123";
    static OdbcConnection con;
    public Last_Event_View()
    {
        //
        // TODO: Add constructor logic here
        //
        con = new OdbcConnection(path);
    }
    public OdbcConnection getconnection()
    {
        //return the created connection
        return con;
    }
    //get all records
    public DataTable getall()
    {
        try
        {
            DataTable dt_all;
            //create new connection with the databse
            OdbcConnection conn = getconnection();
            //open the created connection
            conn.Open();
            //declare dataadpter variable and hold the values 
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from school_events ", conn);
            //create new data tbale object
            dt_all = new DataTable();
            //fill the data table object through data adapter
            adapter.Fill(dt_all);
            //close the open clonnection
            conn.Close();
            //return the filled data table object
            return dt_all;
        }
        catch (Exception ss)
        {
            string s = ss.Message;
            return null;
        }
    }
    //get the search result
    public DataTable search(string key)
    {
        try
        {
            DataTable dt_search;
            //create new connection with the databse
            OdbcConnection conn = getconnection();
            //open the created connection
            conn.Open();
            //declare dataadpter variable and hold the values 
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from school_events where EVENT_NAME like '%" + key + "%'", conn);
            //create new data tbale object
            dt_search = new DataTable();
            //fill the data table object through data adapter
            adapter.Fill(dt_search);
            //close the open clonnection
            conn.Close();
            //return the filled data table object
            return dt_search;
        }
        catch
        {
            return null;
        }

    }

}
